'''
Taken from:
https://www.kaggle.com/akumar6/complete-process-using-resnet-as-a-starting-point
by:
'''

from __future__ import division

import six
import numpy as np
import pandas as pd
import cv2
import glob
import random
import os

from keras.models import Model
from keras.layers import Input, Activation, merge, Dense, Flatten
from keras.layers.normalization import BatchNormalization
from keras.regularizers import l2
from keras import backend as K
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model

np.random.seed(2016)
random.seed(2016)


def batch_generator_train(files, batch_size, conf):
    number_of_batches = np.ceil(len(files)/batch_size)
    counter = 0
    random.shuffle(files)
    while True:
        batch_files = files[batch_size*counter:batch_size*(counter+1)]
        image_list = []
        mask_list = []
        for f in batch_files:
            image = cv2.imread(f)
            image = cv2.resize(image, conf['image_shape'])

            cancer_type = f[20:21]  # relies on path lengths that is hard coded below
            if cancer_type == '1':
                mask = [1, 0, 0]
            elif cancer_type == '2':
                mask = [0, 1, 0]
            else:
                mask = [0, 0, 1]

            image_list.append(image)
            mask_list.append(mask)

        counter += 1
        image_list = np.array(image_list)
        mask_list = np.array(mask_list)

        yield image_list, mask_list

        if counter == number_of_batches:
            random.shuffle(files)
            counter = 0


def create_model(img_rows, img_cols):
    model = Sequential()
    num_channels = 3

    model.add(Conv2D(32, 3, 3, border_mode='same',
                     input_shape=(num_channels, img_rows, img_cols)))

    model.add(Activation('relu'))
    model.add(Conv2D(32, 3, 3))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, 3, 3, border_mode='same'))
    model.add(Activation('relu'))
    model.add(Conv2D(64, 3, 3))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes))
    model.add(Activation('softmax'))

    adam = keras.optimizers.adam(lr=0.0001, decay=1e-6)

    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])

    return model


if __name__ == '__main__':
    conf = dict()

    # How many patients will be in train and validation set during training. Range: (0; 1)
    conf['train_valid_fraction'] = 0.75

    # Batch size for CNN [Depends on GPU and memory available]
    conf['batch_size'] = 100

    # Number of epochs for CNN training
    conf['nb_epoch'] = 10

    # Early stopping. Stop training after epochs without improving on validation
    conf['patience'] = 10

    # Shape of image for CNN (Larger the better, but you need to increase CNN as well)
    conf['image_shape'] = (256, 256)

    filepaths = map(
        lambda x: os.path.join('/ssd/intel/train', x),
        ['Type_1', 'Type_2', 'Type_3']
    )

    allFiles = []
    for i, filepath in enumerate(filepaths):
        files = glob.glob(os.path.join(filepath, '*.jpg'))
        allFiles = allFiles + files

    split_point = int(round(conf['train_valid_fraction']*len(allFiles)))
    random.shuffle(allFiles)

    train_list = allFiles[:split_point]
    valid_list = allFiles[split_point:]
    print('Train patients: {}'.format(len(train_list)))
    print('Valid patients: {}'.format(len(valid_list)))

    model = create_model(*conf['image_shape'])

    callbacks = [
        EarlyStopping(monitor='val_loss', patience=conf['patience'], verbose=0),
        ModelCheckpoint('cervical_best.hdf5', monitor='val_loss', save_best_only=True, verbose=0),
    ]

    print('Fit model...')
    fit = model.fit_generator(
            generator=batch_generator_train(train_list, conf['batch_size']),
            nb_epoch=conf['nb_epoch'],
            # samples_per_epoch=len(train_list),
            samples_per_epoch=3,
            validation_data=batch_generator_train(valid_list, conf['batch_size']),
            # nb_val_samples=len(valid_list),
            nb_val_samples=30,
            verbose=1,
            callbacks=callbacks)
